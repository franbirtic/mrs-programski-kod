#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306_STM32.h>
#include "Adafruit_VEML6070.h"
#include "BlueDot_BME280.h"
#include <SD.h>

#define LIBMAPLE_CORE
#define ENC_CLK PB8
#define ENC_DATA PB9

#define OLED_RESET -1
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

volatile uint32_t menuIndex;
const int SWBtn = PB5;
volatile boolean btnPressed = true;
volatile int rain = 0;
volatile int wind = 0;

volatile unsigned long lastRain = 0;
volatile unsigned long lastWind = 0;
volatile unsigned long firstWind = 0;

unsigned long sunsetTime =0;
unsigned long sunriseTime =0;
String dayString;
boolean isDay = true;
unsigned long dayTime=0;

int loop_count = 0;
int minute_count =0;
int hour_count = 0;
//mjerenje u svakoj sekundi
float temperature[60];
float humidity[60];
float pressure[60];
int light[60];
int UV[60];
//upis prosjeka minute u polje koje predstavlja sat
float temperature_hour[60];
float humidity_hour[60];
float pressure_hour[60];
int UV_hour[60];
int light_hour[60];
int wind_hour[60];
float roshiste;
//vjetar
int windSpeedMax =0;
//kiša
int yesterdaysRain[24];
int rain_hour[24];
unsigned long yesterdaysTotalRain =0;

//prosjek
float temp_avg = 0;
float humid_avg = 0;
float press_avg = 0;
int UV_avg = 0;
float light_avg=0;
//min/max
float temp_max =0;
float temp_min = 0;
float humid_max =0;
float humid_min =0;
float press_max =0;
float press_min =0;
int windmax = 0;
int light_min = 0;
int light_max = 0;
int UV_max=0;
int UV_min=0;

const int lightSensor = PA0; 
const int RainPin = PC14;
const int WindPin = PC15;
Adafruit_VEML6070 uv = Adafruit_VEML6070();
BlueDot_BME280 bme280 = BlueDot_BME280();
File myFile;
const int chipSelect = PA4;

unsigned long previousMillis = 0;

const long interval = 1000;

void setup()   {                
  
  uv.begin(VEML6070_1_T);
  bme280.parameter.communication = 0;
  bme280.parameter.I2CAddress = 0x76;
  bme280.parameter.sensorMode = 0b11;
  bme280.parameter.IIRfilter = 0b100;
  bme280.parameter.humidOversampling = 0b101;
  bme280.parameter.tempOversampling = 0b101;
  bme280.parameter.pressOversampling = 0b101;
  bme280.parameter.pressureSeaLevel = 1013.25;
  bme280.parameter.tempOutsideCelsius = 20;

  bme280.init();

  SD.begin(chipSelect);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3D);
  pinMode(ENC_CLK, INPUT);
  pinMode(ENC_DATA, INPUT);
  pinMode(SWBtn, INPUT);
  attachInterrupt(digitalPinToInterrupt(SWBtn), switchChange, RISING);
  attachInterrupt(digitalPinToInterrupt(RainPin), newRain, RISING);
  attachInterrupt(digitalPinToInterrupt(WindPin), newWind, RISING);
  menuIndex=0;
  systick_attach_callback(&encoder1_read);
  //pinMode(PA9, INPUT_PULLUP);

  Serial.begin(9600);

  display.clearDisplay();
}

void loop() {

unsigned long currentMillis = millis();

if (currentMillis - previousMillis >= interval) {
  
  previousMillis = currentMillis;

  if (Serial.available()>0){
    readUART();
  }
  if (loop_count > 59){
    calcLastMin();
    saveToCard();
    loop_count = 0;
    temperature_hour[minute_count]=temp_avg;
    humidity_hour[minute_count]=humid_avg;

    pressure_hour[minute_count]=press_avg;
    UV_hour[minute_count]=UV_avg;
    light_hour[minute_count]=light_avg;
    if(minute_count == 0){
        if(light_hour[minute_count] >60 && !isDay){
          sunriseTime = millis();
          isDay=true;
          
        }else if(light_hour[minute_count] <60 && isDay){
          sunsetTime = millis();
          isDay=false;
          dayTime=millis()-sunriseTime;
          dayTime = dayTime/60000;
        }
    }else{
        if(light_hour[minute_count] >60 && !isDay){
            sunriseTime = millis();
          isDay=true;
         }else if(light_hour[minute_count] <60 && isDay){
                  sunsetTime = millis();
                  isDay=false;
                  dayTime=millis()-sunriseTime;
                  dayTime = dayTime/60000;
        }
    }

    wind_hour[minute_count] = windSpeedMax;
    windSpeedMax = 0;
    if(minute_count == 0){
        rain_hour[minute_count]=rain - rain_hour[59];
    }else{
        rain_hour[minute_count]=rain - rain_hour[minute_count-1];
    }
    
    minute_count++;
    if (minute_count > 59){
        yesterdaysRain[hour_count]=0;
        for(int i = 0; i<60;i++){
            yesterdaysRain[hour_count]+=rain_hour[i];
        }
        minute_count = 0;
        hour_count++;
        if(hour_count>23){
            hour_count=0;
            yesterdaysTotalRain = 0;
            for(int i = 0; i<24;i++){
            yesterdaysTotalRain+=yesterdaysRain[i];
            }
            yesterdaysTotalRain = yesterdaysTotalRain * (0.006/0.00159);
        }
    }
  }

  temperature[loop_count] = bme280.readTempC();
  humidity[loop_count] = bme280.readHumidity();
  pressure[loop_count] = bme280.readPressure();
  UV[loop_count] = uv.readUV()/1000;
  light[loop_count] = analogRead(lightSensor);
  if(light[loop_count] < 0){
    light[loop_count] = 0;
  }else{
    light[loop_count] = light[loop_count] * 2.2;
  }
  
  loop_count++;
  }
if(millis()-lastRain>18000000  && lastRain != 0){
      lastRain = 0;
      rain =0;
  }
if(millis()-lastWind>5000  && lastWind != 0){
    int windSpeed = 0;
      windSpeed = wind*6/((lastWind-firstWind)/1000);
      if(windSpeed > windSpeedMax)
      windSpeedMax = windSpeed;
      lastWind = 0;
      firstWind = 0;
      wind =0;
  }
  static uint32_t count;
  static uint32_t prevCount; 
  count = menuIndex;
  if (count != prevCount)
  {
    prevCount = count;
  }

 if (btnPressed == true){
 switch(menuIndex){
    
    case 0:
      display_current();
      break;
    case 1:
      calcLastMin();
      printMinMax();
      break;      
    case 2:
      calcNMin(5);
      printMinMax();
      break;
      
    case 3:
      calcNMin(10);
      printMinMax();
      break;
      
    case 4:
      calcNMin(15);
      printMinMax();
      break;

    case 5:
      calcNMin(30);
      printMinMax();
      break;
      
    case 6:
      calcNMin(45);
      printMinMax();
      break;

    case 7:
      calcLast60Min(); 
      printMinMax();
      break;
     
    default:
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.print(menuIndex);
    display.display();
      break;
    }
 }
 else{
    longSwitchFunc();
 }
    
}

void GetData(int n){
    myFile = SD.open("vrime.txt", FILE_READ);
    char str[84];
    int i=0;
    if (myFile) {
      myFile.seek(myFile.size()- n*85);
      
      while(i<84){
        str[i]=myFile.read();
        i++;
      }
    myFile.close();
    Serial.println("successfull read from card");
    menuIndex = -1;
    btnPressed = false;
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.print(str);
    display.display();
    Serial.print(str);
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening vrime.txt");
  }  
}

void display_current(){

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(menuIndex);
  display.print("temp(C):");
  display.println(temperature[loop_count-1]);
  display.print("humid(%):");
  display.println(humidity[loop_count-1]);
  display.print("press (hPa):");
  display.println(pressure[loop_count-1]);
  display.print("UV level : ");
  display.println(UV[loop_count-1]);
  display.print("Light : ");
  display.println(light[loop_count-1]);
  display.print("Rain:");
  display.print(rain*6);
  display.print("mL Wind:");
  display.println(windSpeedMax);
  display.display();
}

void printMinMax(){
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print(menuIndex);display.print("AVG/MIN/MAX");
  display.print(" temp(C):");display.print(temp_avg);display.print("/");display.print(temp_min);display.print("/");display.println(temp_max);
  display.print("hum(C):");display.print(humid_avg);display.print("/");display.print(humid_min);display.print("/");display.print(humid_max);
  display.print(" pres(hPa):");display.println(press_avg);display.print("/");display.print(humid_min);display.print("/");display.print(press_max);
  display.print(" UV avg:");display.print(UV_avg);
  display.display();
  }

void saveToCard(){
    myFile = SD.open("vrime.txt", FILE_WRITE);
    if (myFile) {
    String buf=hour_count+(String)":"+minute_count+(String)" "+temp_avg+(String)"C "+humid_avg+(String)"% "+press_avg+(String)"hPa "+UV_avg+(String)" "+light_avg+(String)"cd "+windSpeedMax+(String)"m/s "+yesterdaysTotalRain+(String)"L/m2 "+(String)"lastDay"+dayTime+(String)"min "+(String)"Sunny:"+isDay+(String)" Ts"+roshiste;
    if(buf.length()<82){
      buf=fixStringLength(buf);
    }
    Serial.println(buf);
    myFile.println(buf);

    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening vrime.txt");
  }
}

String fixStringLength(String str){
  if (str.length()<82){
    while(str.length()<83){
      str+="-";
    }
  }
  return str;
}
void calcLastMin(){
  int i =1;
  temp_avg = 0;
  humid_avg = 0;
  press_avg = 0;
  UV_avg = 0;
  light_avg =0;
  temp_max=temperature[0];
  for (i=0; i<60; i++){
    temp_avg = temp_avg + temperature[i];
    if(temp_max<temperature[i]){
      temp_max=temperature[i];
    }
  }
  temp_avg = temp_avg/(float)60;

  temp_min=temperature[0];
  for (i=1; i<60; i++){
    if(temp_min > temperature[i]){
      temp_min=temperature[i];
    }
  }

  humid_max=humidity[0];
  for (i=0; i<60; i++){
    humid_avg = humid_avg + humidity[i];
    if(humid_max<humidity[i]){
      humid_max=humidity[i];
    }
  }
  humid_avg = humid_avg/60;

  humid_min=humidity[0];
  for (i=1; i<60; i++){
    if(humid_min>humidity[i]){
      humid_min=humidity[i];
    }
  }
  
  float alf=(((17.62*temp_avg)/(243.12+temp_avg))+log(humid_avg/100));
  roshiste= (243.12*alf)/(17.62-alf);
  press_max=pressure[0];
  for (i=0; i<60; i++){
    press_avg = press_avg + pressure[i];
    if(press_max<pressure[i]){
      press_max=pressure[i];
    }
  }
  press_avg = press_avg/60;

  press_min=pressure[0];
  for (i=1; i<60; i++){
    if(press_min>pressure[i]){
      press_min=pressure[i];
    }
  }

  UV_max=UV[0];
  for (i=1; i<60; i++){
    UV_avg = UV_avg + UV[i];
    if(UV_max<UV[i]){
      UV_max=UV[i];
    }
  }
  UV_avg= UV_avg/60;
  UV_min=UV[0];
  for (i=1; i<60; i++){
    if(UV_min>UV[i]){
      UV_min=UV[i];
    }
  }
    for (i=0; i<60; i++){
    light_avg = light_avg + light[i];
    if(light_max<light[i]){
      light_max=light[i];
    }
  }
  light_avg = light_avg/(float)60;

  light_min=light[0];
  for (i=1; i<60; i++){
    if(light_min > light[i]){
      light_min=light[i];
    }
  }
}
void calcLast60Min(){
  int i =1;
  temp_avg = 0;
  humid_avg = 0;
  press_avg = 0;
  UV_avg = 0;
  light_avg =0;
  temp_max=temperature_hour[0];
  for (i=0; i<60; i++){
    temp_avg = temp_avg + temperature_hour[i];
    if(temp_max<temperature_hour[i]){
      temp_max=temperature_hour[i];
    }
  }
  temp_avg = temp_avg/(float)60;

  temp_min=temperature_hour[0];
  for (i=1; i<60; i++){
    if(temp_min > temperature_hour[i]){
      temp_min=temperature_hour[i];
    }
  }

  humid_max=humidity_hour[0];
  for (i=0; i<60; i++){
    humid_avg = humid_avg + humidity_hour[i];
    if(humid_max<humidity_hour[i]){
      humid_max=humidity_hour[i];
    }
  }
  humid_avg = humid_avg/60;

  humid_min=humidity_hour[0];
  for (i=1; i<60; i++){
    if(humid_min>humidity_hour[i]){
      humid_min=humidity_hour[i];
    }
  }

  press_max=pressure_hour[0];
  for (i=0; i<60; i++){
    press_avg = press_avg + pressure_hour[i];
    if(press_max<pressure_hour[i]){
      press_max=pressure_hour[i];
    }
  }
  press_avg = press_avg/60;

  press_min=pressure_hour[0];
  for (i=1; i<60; i++){
    if(press_min>pressure_hour[i]){
      press_min=pressure_hour[i];
    }
  }

  UV_max=UV_hour[0];
  for (i=1; i<60; i++){
    UV_avg = UV_avg + UV_hour[i];
    if(UV_max<UV_hour[i]){
      UV_max=UV_hour[i];
    }
  }
  UV_avg= UV_avg/60;
  UV_min=UV_hour[0];
  for (i=1; i<60; i++){
    if(UV_min>UV_hour[i]){
      UV_min=UV_hour[i];
    }
  }
    for (i=0; i<60; i++){
    light_avg = light_avg + light_hour[i];
    if(light_max<light_hour[i]){
      light_max=light_hour[i];
    }
  }
  light_avg = light_avg/(float)60;

  light_min=light_hour[0];
  for (i=1; i<60; i++){
    if(light_min > light_hour[i]){
      light_min=light_hour[i];
    }
  }
}

void calcNMin(int n){
  int i =1;
  temp_avg = 0;
  humid_avg = 0;
  press_avg = 0;
  UV_avg = 0;
  light_avg = 0;

  
  //sprjecavanje overflowa
  if(minute_count>n-3){
    temp_max=temperature_hour[minute_count-1];
   for (i=minute_count-1; i>(minute_count-n-1); i--){
    temp_avg = temp_avg + temperature_hour[i];
    if(temp_max<temperature_hour[i]){
      temp_max=temperature_hour[i];
    }
  }
  temp_avg = temp_avg/(float)n;

  temp_min=temperature_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    if(temp_min > temperature_hour[i]){
      temp_min=temperature_hour[i];
    }
  }

  humid_max=humidity_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    humid_avg = humid_avg + humidity_hour[i];
    if(humid_max<humidity_hour[i]){
      humid_max=humidity_hour[i];
    }
  }
  humid_avg = humid_avg/(float)n;

  humid_min=humidity_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    if(humid_min>humidity_hour[i]){
      humid_min=humidity_hour[i];
    }
  }

  press_max=pressure_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    press_avg = press_avg + pressure_hour[i];
    if(press_max<pressure_hour[i]){
      press_max=pressure_hour[i];
    }
  }
  press_avg = press_avg/(float)n;

  press_min=pressure_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    if(press_min>pressure_hour[i]){
      press_min=pressure_hour[i];
    }
  }

  UV_max=UV_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    UV_avg = UV_avg + UV_hour[i];
    if(UV_max<UV_hour[i]){
      UV_max=UV_hour[i];
    }
  }
  UV_avg= UV_avg/(float)n;
  UV_min=UV_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    if(UV_min>UV_hour[i]){
      UV_min=UV_hour[i];
    }
  }
  light_max=light_hour[minute_count-1];
   for (i=minute_count-1; i>(minute_count-n-1); i--){
    light_avg = light_avg + light_hour[i];
    if(light_max<light_hour[i]){
      light_max=light_hour[i];
    }
  }
  light_avg = light_avg/(float)n;

  light_min=light_hour[minute_count-1];
  for (i=minute_count-1; i>(minute_count-n-1); i--){
    if(light_min > light_hour[i]){
      light_min=light_hour[i];
    }
  }
  }else{
   i=minute_count-1;
   temp_max=temperature_hour[minute_count-1];
   while(i != (minute_count+(60-n-1))){
    temp_avg = temp_avg + temperature_hour[i];
    if(temp_max<temperature_hour[i]){
      temp_max=temperature_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  temp_avg = temp_avg/(float)n;

  temp_min=temperature_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    if(temp_min > temperature_hour[i]){
      temp_min=temperature_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }

  humid_max=humidity_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    humid_avg = humid_avg + humidity_hour[i];
    if(humid_max<humidity_hour[i]){
      humid_max=humidity_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  humid_avg = humid_avg/(float)n;

  humid_min=humidity_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    Serial.println(humidity_hour[i]);
    if(humid_min>humidity_hour[i]){
      humid_min=humidity_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }

  press_max=pressure_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    press_avg = press_avg + pressure_hour[i];
    if(press_max<pressure_hour[i]){
      press_max=pressure_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  press_avg = press_avg/(float)n;

  press_min=pressure_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    if(press_min>pressure_hour[i]){
      press_min=pressure_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }

  UV_max=UV_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    UV_avg = UV_avg + UV_hour[i];
    if(UV_max<UV_hour[i]){
      UV_max=UV_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  UV_avg= UV_avg/(float)n;
  
  UV_min=UV_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    if(UV_min>UV_hour[i]){
      UV_min=UV_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  i=minute_count-1;
  light_max=light_hour[minute_count-1];
   while(i != (minute_count+(60-n-1))){
    light_avg = light_avg + light_hour[i];
    if(light_max<light_hour[i]){
      light_max=light_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  light_avg = light_avg/(float)n;

  light_min=light_hour[minute_count-1];
  i=minute_count-1;
  while(i != (minute_count+(60-n-1))){
    if(light_min > light_hour[i]){
      light_min=light_hour[i];
    }
    i--;
    if(i<0){
      i=59;
    }
  }
  }

  printMinMax();
}

void encoder1_read(void)
{
  volatile static uint8_t ABs = 0;
  ABs = (ABs << 2) & 0x0f; //left 2 bits now contain the previous AB key read-out;
  ABs |= (digitalRead(ENC_CLK) << 1) | digitalRead(ENC_DATA);
  switch (ABs)
  {
    case 0x0d:
      menuIndex++;
      break;
    case 0x0e:
      menuIndex--;
      break;
  }
}

void switchChange(void){
    
    if(btnPressed == true){
      btnPressed = false;
    }else{
      menuIndex=0;
      btnPressed = true;
    }
}
void readUART(){
  String t; //string to hold data from BT module
  while(Serial.available()>0) { //keep reading bytes while they are still more in the buffer
    t += (char)Serial.read(); //read byte, convert to char, and append it to string
  }
  Serial.print("Primljena naredba : ");
  Serial.println(t);
  
    if(t == "get1\r\n") { 
      calcLastMin();
      Serial.println("Prikaz 1 min");
      printAvgUART();
      }
    else if(t == "get5\r\n") {
      calcNMin(5);
      Serial.print("Prikaz 5 min\n");
      printAvgUART();
   }
    else if(t == "get10\r\n") {
      calcNMin(10);
      Serial.print("Prikaz 10 min\n");
      printAvgUART();
   }
    else if(t == "get15\r\n") {
      calcNMin(15);
      Serial.print("Prikaz 15 min\n");
      printAvgUART();
   }
    else if(t == "get30\r\n") {
        calcNMin(30);
        Serial.print("Prikaz 30 min\n");
        printAvgUART();
   }
    else if(t == "get45\r\n") {
      calcNMin(45);
      Serial.print("Prikaz 45 min\n");
      printAvgUART();
   }
    else if(t == "get60\r\n") {
      calcLast60Min();
      Serial.print("Prikaz 60  min\n");
      printAvgUART();
   }else if(t == "data1\r\n") {
      GetData(1);
   }else if(t == "data2\r\n") {
      GetData(2);
   }else if(t == "data3\r\n") {
      GetData(3);
   }else if(t == "data4\r\n") {
      GetData(4);
   }else if(t == "data5\r\n") {
      GetData(5);
   }else if(t == "data6\r\n") {
      GetData(6);
   }else if(t == "data7\r\n") {
      GetData(7);
   }else if(t == "data8\r\n") {
      GetData(8);
   }else if(t == "data9\r\n") {
      GetData(9);
   }else if(t == "data10\r\n") {
      GetData(10);
   }else if(t == "data11\r\n") {
      GetData(11);
   }else if(t == "data12\r\n") {
      GetData(12);
   }else if(t == "data13\r\n") {
      GetData(13);
   }else if(t == "data14\r\n") {
      GetData(14);
   }else if(t == "data15\r\n") {
      GetData(15);
   }else if(t == "data16\r\n") {
      GetData(16);
   }
   else { Serial.print("Syntax Error\n"); }
}

void printAvgUART(){
    Serial.println("temperature avg/max/min ="+(String)temp_avg+"/"+(String)temp_max+"/"+(String)temp_min);
    Serial.println("humidiyty avg/max/min ="+(String)humid_avg+"/"+(String)humid_max+"/"+(String)humid_min);
    Serial.println("pressure avg/max/min ="+(String)press_avg+"/"+(String)press_max+"/"+(String)press_min);
    Serial.println("UV avg/max/min ="+(String)UV_avg+"/"+(String)UV_max+"/"+(String)UV_min);
    Serial.println("light avg/max/min ="+(String)light_avg+"/"+(String)light_max+"/"+(String)light_min);
}

void newRain(){
    rain++;
    lastRain = millis();
}

void newWind(){
  wind++;
  lastWind = millis();
  if(firstWind == 0){
    firstWind=lastWind;
  }
}

void longSwitchFunc(){
switch(menuIndex){
  default:
    break;
case 0:
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Menu 2");
  display.display();
case 1:
  GetData(1);
  break;
case 2:
  GetData(2);
  break;
case 3:
  GetData(3);
  break;
case 4:
  GetData(4);
  break;
case 5:
  GetData(5);
  break;
case 6:
  GetData(6);
  break;
case 7:
  GetData(7);
  break;
case 8:
  GetData(8);
  break;
case 9:
  GetData(9);
  break;
case 10:
  GetData(10);
  break;
case 11:
  GetData(11);
  break;
case 12:
  GetData(12);
  break;
case 13:
  GetData(13);
  break;
case 14:
  GetData(14);
  break;
case 15:
  GetData(15);
  break;
case 16:
  GetData(16);
  break;
case 17:
  GetData(17);
  break;
case 18:
  GetData(18);
  break;
case 19:
  GetData(19);
  break;
case 20:
  GetData(20);
  break;
case 21:
  GetData(21);
  break;
case 22:
  GetData(22);
  break;
case 23:
  GetData(23);
  break;
case 24:
  GetData(24);
  break;
case 25:
  GetData(25);
  break;
case 26:
  GetData(26);
  break;
case 27:
  GetData(27);
  break;
case 28:
  GetData(28);
  break;
case 29:
  GetData(29);
  break;
case 30:
  GetData(30);
  break;
case 31:
  GetData(31);
  break;
case 32:
  GetData(32);
  break;
case 33:
  GetData(33);
  break;
case 34:
  GetData(34);
  break;
case 35:
  GetData(35);
  break;
case 36:
  GetData(36);
  break;
case 37:
  GetData(37);
  break;
case 38:
  GetData(38);
  break;
case 39:
  GetData(39);
  break;
case 40:
  GetData(40);
  break;
case 41:
  GetData(41);
  break;
case 42:
  GetData(42);
  break;
case 43:
  GetData(43);
  break;
case 44:
GetData(44);
  break;
case 45:
  GetData(45);
  break;
case 46:
  GetData(46);
  break;
case 47:
  GetData(47);
  break;
case 48:
  GetData(48);
  break;
case 49:
  GetData(49);
  break;
case 50:
  GetData(50);
  break;
case 51:
  GetData(51);
  break;
case 52:
  GetData(52);
  break;
case 53:
  GetData(53);
  break;
case 54:
  GetData(54);
  break;
case 55:
  GetData(55);
  break;
case 56:
  GetData(56);
  break;
case 57:
  GetData(57);
  break;
case 58:
  GetData(58);
  break;
case 59:
  GetData(59);
  break;
case 60:
  GetData(60);
  break;
case 61:
  GetData(61);
  break;
case 62:
  GetData(62);
  break;
case 63:
  GetData(63);
  break;
case 64:
  GetData(64);
  break;
case 65:
  GetData(65);
  break;
case 66:
  GetData(66);
  break;
case 67:
  GetData(67);
  break;
case 68:
  GetData(68);
  break;
case 69:
  GetData(69);
  break;
case 70:
  GetData(70);
  break;
case 71:
  GetData(71);
  break;
case 72:
  GetData(72);
  break;
case 73:
  GetData(73);
  break;
case 74:
  GetData(74);
  break;
case 75:
  GetData(75);
  break;
case 76:
  GetData(76);
  break;
case 77:
  GetData(77);
  break;
case 78:
  GetData(78);
  break;
case 79:
  GetData(79);
  break;
case 80:
  GetData(80);
  break;
case 81:
  GetData(81);
  break;
case 82:
  GetData(82);
  break;
case 83:
  GetData(83);
  break;
case 84:
  GetData(84);
  break;
case 85:
  GetData(85);
  break;
case 86:
  GetData(86);
  break;
case 87:
  GetData(87);
  break;
case 88:
  GetData(88);
  break;
case 89:
  GetData(89);
  break;
case 90:
  GetData(90);
  break;
case 91:
  GetData(91);
  break;
case 92:
  GetData(92);
  break;
case 93:
  GetData(93);
  break;
case 94:
  GetData(94);
  break;
case 95:
  GetData(95);
  break;
case 96:
  GetData(96);
  break;
case 97:
  GetData(97);
  break;
case 98:
  GetData(98);
  break;
case 99:
  GetData(99);
  break;
case 100:
  GetData(100);
  break;
case 101:
  GetData(101);
  break;
case 102:
  GetData(102);
  break;
case 103:
  GetData(103);
  break;
case 104:
  GetData(104);
  break;
case 105:
  GetData(105);
  break;
case 106:
  GetData(106);
  break;
case 107:
  GetData(107);
  break;
case 108:
  GetData(108);
  break;
case 109:
  GetData(109);
  break;
case 110:
  GetData(110);
  break;
case 111:
  GetData(111);
  break;
case 112:
  GetData(112);
  break;
case 113:
  GetData(113);
  break;
case 114:
  GetData(114);
  break;
case 115:
  GetData(115);
  break;
case 116:
  GetData(116);
  break;
case 117:
  GetData(117);
  break;
case 118:
  GetData(118);
  break;
case 119:
  GetData(119);
  break;
case 120:
  GetData(120);
  break;
case 121:
  GetData(121);
  break;
case 122:
  GetData(122);
  break;
case 123:
  GetData(123);
  break;
case 124:
  GetData(124);
  break;
case 125:
  GetData(125);
  break;
case 126:
  GetData(126);
  break;
case 127:
  GetData(127);
  break;
case 128:
  GetData(128);
  break;
case 129:
  GetData(129);
  break;
case 130:
  GetData(130);
  break;
case 131:
  GetData(131);
  break;
case 132:
  GetData(132);
  break;
case 133:
  GetData(133);
  break;
case 134:
  GetData(134);
  break;
case 135:
  GetData(135);
  break;
case 136:
  GetData(136);
  break;
case 137:
  GetData(137);
  break;
case 138:
  GetData(138);
  break;
case 139:
  GetData(139);
  break;
case 140:
  GetData(140);
  break;
case 141:
  GetData(141);
  break;
case 142:
  GetData(142);
  break;
case 143:
  GetData(143);
  break;
case 144:
  GetData(144);
  break;
case 145:
  GetData(145);
  break;
case 146:
  GetData(146);
  break;
case 147:
  GetData(147);
  break;
case 148:
  GetData(148);
break;
case 149:
  GetData(149);
  break;
case 150:
  GetData(150);
  break;
case 151:
  GetData(151);
  break;
case 152:
  GetData(152);
  break;
case 153:
  GetData(153);
  break;
case 154:
  GetData(154);
  break;
case 155:
  GetData(155);
  break;
case 156:
  GetData(156);
  break;
case 157:
  GetData(157);
  break;
case 158:
  GetData(158);
  break;
case 159:
  GetData(159);
  break;
case 160:
  GetData(160);
  break;
case 161:
  GetData(161);
  break;
case 162:
  GetData(162);
  break;
case 163:
  GetData(163);
  break;
case 164:
  GetData(164);
  break;
case 165:
  GetData(165);
  break;
case 166:
  GetData(166);
  break;
case 167:
  GetData(167);
  break;
case 168:
  GetData(168);
  break;
case 169:
  GetData(169);
  break;
case 170:
  GetData(170);
  break;
case 171:
  GetData(171);
  break;
case 172:
  GetData(172);
  break;
case 173:
  GetData(173);
  break;
case 174:
  GetData(174);
  break;
case 175:
  GetData(175);
  break;
case 176:
  GetData(176);
  break;
case 177:
  GetData(177);
  break;
case 178:
  GetData(178);
  break;
case 179:
  GetData(179);
  break;
case 180:
  GetData(180);
  break;
case 181:
  GetData(181);
  break;
case 182:
  GetData(182);
  break;
case 183:
  GetData(183);
  break;
case 184:
  GetData(184);
  break;
case 185:
  GetData(185);
  break;
case 186:
  GetData(186);
  break;
case 187:
  GetData(187);
  break;
case 188:
  GetData(188);
  break;
case 189:
  GetData(189);
  break;
case 190:
  GetData(190);
  break;
case 191:
  GetData(191);
  break;
case 192:
  GetData(192);
  break;
case 193:
  GetData(193);
  break;
case 194:
  GetData(194);
  break;
case 195:
  GetData(195);
  break;
case 196:
  GetData(196);
  break;
case 197:
  GetData(197);
  break;
case 198:
  GetData(198);
  break;
case 199:
  GetData(199);
  break;
case 200:
  GetData(200);
  break;
case 201:
  GetData(201);
  break;
case 202:
  GetData(202);
break;
case 203:
  GetData(203);
  break;
case 204:
  GetData(204);
  break;
case 205:
  GetData(205);
  break;
case 206:
  GetData(206);
  break;
case 207:
  GetData(207);
  break;
case 208:
  GetData(208);
  break;
case 209:
  GetData(209);
  break;
case 210:
  GetData(210);
  break;
case 211:
  GetData(211);
  break;
case 212:
  GetData(212);
  break;
case 213:
  GetData(213);
  break;
case 214:
  GetData(214);
  break;
case 215:
  GetData(215);
  break;
case 216:
  GetData(216);
  break;
case 217:
  GetData(217);
  break;
case 218:
  GetData(218);
  break;
case 219:
  GetData(219);
  break;
case 220:
  GetData(220);
  break;
case 221:
  GetData(221);
  break;
case 222:
  GetData(222);
  break;
case 223:
  GetData(223);
  break;
case 224:
  GetData(224);
  break;
case 225:
  GetData(225);
  break;
case 226:
  GetData(226);
  break;
case 227:
  GetData(227);
  break;
case 228:
  GetData(228);
  break;
case 229:
  GetData(229);
  break;
case 230:
  GetData(230);
  break;
case 231:
  GetData(231);
  break;
case 232:
  GetData(232);
  break;
case 233:
  GetData(233);
  break;
case 234:
  GetData(234);
  break;
case 235:
  GetData(235);
  break;
case 236:
  GetData(236);
  break;
case 237:
  GetData(237);
  break;
case 238:
  GetData(238);
  break;
case 239:
  GetData(239);
  break;
case 240:
  GetData(240);
  break;
case 241:
  GetData(241);
  break;
case 242:
  GetData(242);
  break;
case 243:
  GetData(243);
  break;
case 244:
  GetData(244);
  break;
case 245:
  GetData(245);
  break;
case 246:
  GetData(246);
  break;
case 247:
  GetData(247);
  break;
case 248:
  GetData(248);
  break;
case 249:
GetData(249);
  break;
case 250:
  GetData(250);
  break;
case 251:
  GetData(251);
  break;
case 252:
  GetData(252);
  break;
case 253:
  GetData(253);
  break;
case 254:
  GetData(254);
  break;
case 255:
  GetData(255);
  break;
case 256:
  GetData(256);
  break;
case 257:
  GetData(257);
  break;
case 258:
  GetData(258);
  break;
case 259:
  GetData(259);
  break;
case 260:
  GetData(260);
  break;
case 261:
  GetData(261);
  break;
case 262:
  GetData(262);
  break;
case 263:
  GetData(263);
  break;
case 264:
  GetData(264);
  break;
case 265:
  GetData(265);
  break;
case 266:
  GetData(266);
  break;
case 267:
  GetData(267);
  break;
case 268:
  GetData(268);
  break;
case 269:
  GetData(269);
break;
case 270:
  GetData(270);
  break;
case 271:
  GetData(271);
  break;
case 272:
  GetData(272);
  break;
case 273:
  GetData(273);
  break;
case 274:
  GetData(274);
  break;
case 275:
  GetData(275);
  break;
case 276:
  GetData(276);
  break;
case 277:
  GetData(277);
  break;
case 278:
  GetData(278);
  break;
case 279:
  GetData(279);
  break;
case 280:
  GetData(280);
  break;
case 281:
  GetData(281);
  break;
case 282:
  GetData(282);
  break;
case 283:
  GetData(283);
  break;
case 284:
  GetData(284);
  break;
case 285:
  GetData(285);
  break;
case 286:
  GetData(286);
  break;
case 287:
  GetData(287);
  break;
case 288:
  GetData(288);
  break;
case 289:
  GetData(289);
  break;
case 290:
  GetData(290);
  break;
case 291:
  GetData(291);
break;
case 292:
  GetData(292);
  break;
case 293:
  GetData(293);
  break;
case 294:
  GetData(294);
  break;
case 295:
  GetData(295);
  break;
case 296:
  GetData(296);
  break;
case 297:
  GetData(297);
  break;
case 298:
  GetData(298);
  break;
case 299:
  GetData(299);
  break;
case 300:
  GetData(300);
  break;
case 301:
  GetData(301);
  break;
case 302:
  GetData(302);
  break;
case 303:
  GetData(303);
  break;
case 304:
  GetData(304);
  break;
case 305:
  GetData(305);
  break;
case 306:
  GetData(306);
  break;
case 307:
  GetData(307);
  break;
case 308:
  GetData(308);
  break;
case 309:
  GetData(309);
  break;
case 310:
  GetData(310);
  break;
case 311:
  GetData(311);
  break;
case 312:
  GetData(312);
  break;
case 313:
  GetData(313);
  break;
case 314:
  GetData(314);
  break;
case 315:
  GetData(315);
  break;
case 316:
  GetData(316);
  break;
case 317:
  GetData(317);
  break;
case 318:
  GetData(318);
  break;
case 319:
  GetData(319);
  break;
case 320:
  GetData(320);
  break;
case 321:
  GetData(321);
  break;
case 322:
  GetData(322);
  break;
case 323:
  GetData(323);
  break;
case 324:
  GetData(324);
  break;
case 325:
  GetData(325);
  break;
case 326:
  GetData(326);
  break;
case 327:
  GetData(327);
  break;
case 328:
  GetData(328);
  break;
case 329:
  GetData(329);
  break;
case 330:
  GetData(330);
  break;
case 331:
  GetData(331);
  break;
case 332:
  GetData(332);
  break;
case 333:
  GetData(333);
  break;
case 334:
  GetData(334);
  break;
case 335:
  GetData(335);
  break;
case 336:
  GetData(336);
  break;
case 337:
  GetData(337);
  break;
case 338:
  GetData(338);
  break;
case 339:
  GetData(339);
  break;
case 340:
  GetData(340);
  break;
case 341:
  GetData(341);
break;
case 342:
  GetData(342);
  break;
case 343:
  GetData(343);
  break;
case 344:
  GetData(344);
  break;
case 345:
  GetData(345);
  break;
case 346:
  GetData(346);
  break;
case 347:
  GetData(347);
  break;
case 348:
  GetData(348);
  break;
case 349:
  GetData(349);
  break;
case 350:
  GetData(350);
  break;
case 351:
  GetData(351);
  break;
case 352:
  GetData(352);
  break;
case 353:
  GetData(353);
  break;
case 354:
  GetData(354);
  break;
case 355:
  GetData(355);
  break;
case 356:
  GetData(356);
  break;
case 357:
  GetData(357);
  break;
case 358:
  GetData(358);
  break;
case 359:
  GetData(359);
  break;
case 360:
  GetData(360);
  break;
case 361:
  GetData(361);
  break;
case 362:
  GetData(362);
  break;
case 363:
  GetData(363);
  break;
case 364:
  GetData(364);
  break;
case 365:
  GetData(365);
  break;
case 366:
  GetData(366);
  break;
case 367:
  GetData(367);
  break;
case 368:
  GetData(368);
  break;
case 369:
  GetData(369);
  break;
case 370:
  GetData(370);
  break;
case 371:
  GetData(371);
  break;
case 372:
  GetData(372);
  break;
case 373:
  GetData(373);
  break;
case 374:
  GetData(374);
  break;
case 375:
  GetData(375);
  break;
case 376:
  GetData(376);
  break;
case 377:
  GetData(377);
  break;
case 378:
  GetData(378);
  break;
case 379:
  GetData(379);
  break;
case 380:
  GetData(380);
  break;
case 381:
  GetData(381);
  break;
case 382:
  GetData(382);
  break;
case 383:
  GetData(383);
  break;
case 384:
  GetData(384);
  break;
case 385:
  GetData(385);
  break;
case 386:
  GetData(386);
  break;
case 387:
  GetData(387);
  break;
case 388:
  GetData(388);
  break;
case 389:
  GetData(389);
  break;
case 390:
  GetData(390);
  break;
case 391:
  GetData(391);
  break;
case 392:
  GetData(392);
  break;
case 393:
  GetData(393);
  break;
case 394:
  GetData(394);
  break;
case 395:
  GetData(395);
  break;
case 396:
  GetData(396);
  break;
case 397:
  GetData(397);
  break;
case 398:
  GetData(398);
  break;
case 399:
  GetData(399);
  break;
case 400:
  GetData(400);
  break;
case 401:
  GetData(401);
  break;
case 402:
  GetData(402);
  break;
case 403:
  GetData(403);
  break;
case 404:
  GetData(404);
  break;
case 405:
  GetData(405);
  break;
case 406:
  GetData(406);
  break;
case 407:
  GetData(407);
  break;
case 408:
  GetData(408);
  break;
case 409:
  GetData(409);
  break;
case 410:
  GetData(410);
  break;
case 411:
  GetData(411);
  break;
case 412:
  GetData(412);
  break;
case 413:
  GetData(413);
  break;
case 414:
  GetData(414);
  break;
case 415:
  GetData(415);
  break;
case 416:
  GetData(416);
  break;
case 417:
  GetData(417);
  break;
case 418:
  GetData(418);
  break;
case 419:
  GetData(419);
  break;
case 420:
  GetData(420);
  break;
case 421:
  GetData(421);
  break;
case 422:
  GetData(422);
  break;
case 423:
  GetData(423);
  break;
case 424:
  GetData(424);
  break;
case 425:
  GetData(425);
  break;
case 426:
  GetData(426);
  break;
case 427:
  GetData(427);
  break;
case 428:
  GetData(428);
  break;
case 429:
  GetData(429);
  break;
case 430:
  GetData(430);
  break;
case 431:
  GetData(431);
  break;
case 432:
  GetData(432);
  break;
case 433:
  GetData(433);
  break;
case 434:
  GetData(434);
  break;
case 435:
  GetData(435);
  break;
case 436:
  GetData(436);
  break;
case 437:
  GetData(437);
  break;
case 438:
  GetData(438);
  break;
case 439:
  GetData(439);
  break;
case 440:
  GetData(440);
  break;
case 441:
  GetData(441);
  break;
case 442:
  GetData(442);
  break;
case 443:
  GetData(443);
  break;
case 444:
  GetData(444);
  break;
case 445:
  GetData(445);
  break;
case 446:
  GetData(446);
  break;
case 447:
  GetData(447);
  break;
case 448:
  GetData(448);
  break;
case 449:
  GetData(449);
  break;
case 450:
  GetData(450);
  break;
case 451:
  GetData(451);
  break;
case 452:
  GetData(452);
  break;
case 453:
  GetData(453);
  break;
case 454:
  GetData(454);
  break;
case 455:
  GetData(455);
  break;
case 456:
  GetData(456);
  break;
case 457:
  GetData(457);
  break;
case 458:
  GetData(458);
  break;
case 459:
  GetData(459);
  break;
case 460:
  GetData(460);
  break;
case 461:
  GetData(461);
  break;
case 462:
  GetData(462);
  break;
case 463:
  GetData(463);
  break;
case 464:
  GetData(464);
  break;
case 465:
  GetData(465);
  break;
case 466:
  GetData(466);
  break;
case 467:
  GetData(467);
  break;
case 468:
  GetData(468);
  break;
case 469:
  GetData(469);
  break;
case 470:
  GetData(470);
  break;
case 471:
  GetData(471);
  break;
case 472:
  GetData(472);
  break;
case 473:
  GetData(473);
  break;
case 474:
  GetData(474);
  break;
case 475:
  GetData(475);
  break;
case 476:
  GetData(476);
  break;
case 477:
  GetData(477);
  break;
case 478:
  GetData(478);
  break;
case 479:
  GetData(479);
  break;
case 480:
  GetData(480);
  break;
case 481:
  GetData(481);
  break;
case 482:
  GetData(482);
  break;
case 483:
  GetData(483);
  break;
case 484:
  GetData(484);
  break;
case 485:
  GetData(485);
  break;
case 486:
  GetData(486);
  break;
case 487:
  GetData(487);
  break;
case 488:
  GetData(488);
  break;
case 489:
  GetData(489);
  break;
case 490:
  GetData(490);
  break;
case 491:
  GetData(491);
  break;
case 492:
  GetData(492);
  break;
case 493:
  GetData(493);
  break;
case 494:
  GetData(494);
break;
case 495:
  GetData(495);
  break;
case 496:
  GetData(496);
  break;
case 497:
  GetData(497);
  break;
case 498:
  GetData(498);
  break;
case 499:
  GetData(499);
  break;
case 500:
  GetData(500);
  break;
case 501:
  GetData(501);
  break;
case 502:
  GetData(502);
  break;
case 503:
  GetData(503);
  break;
case 504:
  GetData(504);
  break;
case 505:
  GetData(505);
  break;
case 506:
  GetData(506);
  break;
case 507:
  GetData(507);
  break;
case 508:
  GetData(508);
  break;
case 509:
  GetData(509);
  break;
case 510:
  GetData(510);
  break;
case 511:
  GetData(511);
  break;
case 512:
  GetData(512);
  break;
case 513:
  GetData(513);
  break;
case 514:
GetData(514);
  break;
case 515:
  GetData(515);
  break;
case 516:
  GetData(516);
  break;
case 517:
  GetData(517);
  break;
case 518:
  GetData(518);
  break;
case 519:
  GetData(519);
  break;
case 520:
  GetData(520);
  break;
case 521:
  GetData(521);
  break;
case 522:
  GetData(522);
  break;
case 523:
  GetData(523);
  break;
case 524:
  GetData(524);
  break;
case 525:
  GetData(525);
  break;
case 526:
  GetData(526);
  break;
case 527:
  GetData(527);
  break;
case 528:
  GetData(528);
  break;
case 529:
  GetData(529);
  break;
case 530:
  GetData(530);
  break;
case 531:
  GetData(531);
  break;
case 532:
  GetData(532);
  break;
case 533:
  GetData(533);
  break;
case 534:
  GetData(534);
  break;
case 535:
  GetData(535);
  break;
case 536:
  GetData(536);
  break;
case 537:
  GetData(537);
  break;
case 538:
  GetData(538);
  break;
case 539:
  GetData(539);
  break;
case 540:
  GetData(540);
  break;
case 541:
  GetData(541);
  break;
case 542:
  GetData(542);
  break;
case 543:
  GetData(543);
  break;
case 544:
  GetData(544);
  break;
case 545:
  GetData(545);
  break;
case 546:
  GetData(546);
  break;
case 547:
  GetData(547);
  break;
case 548:
  GetData(548);
  break;
case 549:
  GetData(549);
  break;
case 550:
  GetData(550);
  break;
case 551:
  GetData(551);
  break;
case 552:
  GetData(552);
  break;
case 553:
  GetData(553);
  break;
case 554:
  GetData(554);
  break;
case 555:
  GetData(555);
  break;
case 556:
  GetData(556);
  break;
case 557:
  GetData(557);
  break;
case 558:
  GetData(558);
  break;
case 559:
  GetData(559);
  break;
case 560:
  GetData(560);
  break;
case 561:
  GetData(561);
  break;
case 562:
  GetData(562);
  break;
case 563:
  GetData(563);
  break;
case 564:
  GetData(564);
  break;
case 565:
  GetData(565);
  break;
case 566:
GetData(566);
  break;
case 567:
  GetData(567);
  break;
case 568:
  GetData(568);
  break;
case 569:
  GetData(569);
  break;
case 570:
  GetData(570);
  break;
case 571:
  GetData(571);
  break;
case 572:
  GetData(572);
  break;
case 573:
  GetData(573);
  break;
case 574:
  GetData(574);
  break;
case 575:
  GetData(575);
  break;
case 576:
  GetData(576);
  break;
case 577:
  GetData(577);
  break;
case 578:
  GetData(578);
  break;
case 579:
  GetData(579);
  break;
case 580:
  GetData(580);
  break;
case 581:
  GetData(581);
  break;
case 582:
  GetData(582);break;
case 583:
  GetData(583);
  break;
case 584:
  GetData(584);
  break;
case 585:
  GetData(585);
  break;
case 586:
  GetData(586);
  break;
case 587:
  GetData(587);
  break;
case 588:
  GetData(588);
  break;
case 589:
  GetData(589);
  break;
case 590:
  GetData(590);
  break;
case 591:
  GetData(591);
  break;
case 592:
  GetData(592);
  break;
case 593:
  GetData(593);
  break;
case 594:
  GetData(594);
  break;
case 595:
  GetData(595);
  break;
case 596:
  GetData(596);
  break;
case 597:
  GetData(597);
break;
case 598:
  GetData(598);
  break;
case 599:
  GetData(599);
  break;
case 600:
  GetData(600);
  break;
case 601:
  GetData(601);
  break;
case 602:
  GetData(602);
  break;
case 603:
  GetData(603);
  break;
case 604:
  GetData(604);
  break;
case 605:
  GetData(605);
  break;
case 606:
  GetData(606);
  break;
case 607:
  GetData(607);
  break;
case 608:
  GetData(608);
  break;
case 609:
  GetData(609);
  break;
case 610:
  GetData(610);
  break;
case 611:
  GetData(611);
  break;
case 612:
GetData(612);
  break;
case 613:
  GetData(613);
  break;
case 614:
  GetData(614);
  break;
case 615:
  GetData(615);
  break;
case 616:
  GetData(616);
  break;
case 617:
  GetData(617);
  break;
case 618:
  GetData(618);
  break;
case 619:
  GetData(619);
  break;
case 620:
  GetData(620);
  break;
case 621:
  GetData(621);
  break;
case 622:
  GetData(622);
  break;
case 623:
  GetData(623);
  break;
case 624:
  GetData(624);
  break;
case 625:
  GetData(625);
  break;
case 626:
  GetData(626);
  break;
case 627:
  GetData(627);
  break;
case 628:
  GetData(628);
  break;
case 629:
  GetData(629);
  break;
case 630:
  GetData(630);
  break;
case 631:
  GetData(631);
  break;
case 632:
  GetData(632);
  break;
case 633:
  GetData(633);
  break;
case 634:
  GetData(634);
  break;
case 635:
  GetData(635);
  break;
case 636:
  GetData(636);
  break;
case 637:
  GetData(637);
  break;
case 638:
  GetData(638);
  break;
case 639:
  GetData(639);
  break;
case 640:
  GetData(640);
  break;
case 641:
  GetData(641);
  break;
case 642:
  GetData(642);
break;
case 643:
  GetData(643);
  break;
case 644:
  GetData(644);
  break;
case 645:
  GetData(645);
  break;
case 646:
  GetData(646);
  break;
case 647:
  GetData(647);
  break;
case 648:
  GetData(648);
  break;
case 649:
  GetData(649);
  break;
case 650:
  GetData(650);
  break;
case 651:
  GetData(651);
  break;
case 652:
  GetData(652);
  break;
case 653:
  GetData(653);
  break;
case 654:
  GetData(654);
  break;
case 655:
  GetData(655);
  break;
case 656:
  GetData(656);
  break;
case 657:
  GetData(657);
break;
case 658:
  GetData(658);
  break;
case 659:
  GetData(659);
  break;
case 660:
  GetData(660);
  break;
case 661:
  GetData(661);
  break;
case 662:
  GetData(662);
  break;
case 663:
  GetData(663);
  break;
case 664:
  GetData(664);
  break;
case 665:
  GetData(665);
  break;
case 666:
  GetData(666);
  break;
case 667:
  GetData(667);
  break;
case 668:
  GetData(668);
  break;
case 669:
  GetData(669);
  break;
case 670:
  GetData(670);
  break;
case 671:
  GetData(671);
  break;
case 672:
  GetData(672);
  break;
case 673:
  GetData(673);
  break;
case 674:
  GetData(674);
  break;
case 675:
  GetData(675);
  break;
case 676:
  GetData(676);
  break;
case 677:
  GetData(677);
  break;
case 678:
  GetData(678);
  break;
case 679:
  GetData(679);
  break;
case 680:
  GetData(680);
  break;
case 681:GetData(681);
  break;
case 682:
  GetData(682);
  break;
case 683:
  GetData(683);
  break;
case 684:
  GetData(684);
  break;
case 685:
  GetData(685);
  break;
case 686:
  GetData(686);
  break;
case 687:
  GetData(687);
  break;
case 688:
  GetData(688);
  break;
case 689:
  GetData(689);
  break;
case 690:
  GetData(690);
  break;
case 691:
  GetData(691);
  break;
case 692:
GetData(692);
  break;
case 693:
  GetData(693);
  break;
case 694:
  GetData(694);
  break;
case 695:
  GetData(695);
  break;
case 696:
  GetData(696);
  break;
case 697:
  GetData(697);
  break;
case 698:
  GetData(698);
  break;
case 699:
  GetData(699);
  break;
case 700:
  GetData(700);
  break;
case 701:
  GetData(701);
  break;
case 702:
  GetData(702);
  break;
case 703:
  GetData(703);
  break;
case 704:
  GetData(704);
  break;
case 705:
  GetData(705);
break;
case 706:
  GetData(706);
  break;
case 707:
  GetData(707);
  break;
case 708:
  GetData(708);
  break;
case 709:
  GetData(709);
  break;
case 710:
  GetData(710);
  break;
case 711:
  GetData(711);
  break;
case 712:
  GetData(712);
  break;
case 713:
  GetData(713);
  break;
case 714:
  GetData(714);
  break;
case 715:
  GetData(715);
  break;
case 716:
  GetData(716);
  break;
case 717:
  GetData(717);
  break;
case 718:
  GetData(718);
break;
case 719:
  GetData(719);
  break;
case 720:
  GetData(720);
  break;
}
}
